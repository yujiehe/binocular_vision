# Link Collection

## Journal & Conference Papers

- [SCI收录期刊——机器人学科](http://blog.sciencenet.cn/home.php?mod=space&uid=57081&do=blog&id=214037)
- [机器人领域最好的会议是什么?](https://www.zhihu.com/question/266477032/answer/308500377)
- IJRR
- [Robotics: Science and Systems](http://www.roboticsconference.org/)

http://www.roboticsproceedings.org/

## Tracking

### csdn

- [2017目标跟踪算法综述](https://blog.csdn.net/huixingshao/article/details/77150563)
- [计算机视觉中，目前有哪些经典的目标跟踪算法？](http://www.sohu.com/a/230480221_633698)

<<<<<<< HEAD
### Lab

- http://rpg.ifi.uzh.ch/

## Datasets & Lib

### dataset and its use

- [KITTI](http://www.cvlibs.net/datasets/kitti/eval_object.php)
- [Cityscapes](https://www.cityscapes-dataset.com/dataset-overview/)
- [EUROC MAV](https://projects.asl.ethz.ch/datasets/doku.php?id=kmavvisualinertialdatasets)

[KITTI数据集简介与使用](https://blog.csdn.net/solomon1558/article/details/70173223)

[KITTI与Cityscapes简介](https://blog.csdn.net/u010368556/article/details/78256886)

[slam数据集整合](https://blog.csdn.net/aaronmorgan/article/details/77184591)

### lib

- g2o
- Eigen
- pcl
- gtsam
- openCV

## Codes

### gitxiv

- [orb-slam-a-versatile-and-accurate-monocular-slam-system](http://www.gitxiv.com/posts/hJ9Q8q66GF9WRgasY/orb-slam-a-versatile-and-accurate-monocular-slam-system)
- [orb-slam2-an-open-source-slam-system-for-monocular-stereo](http://www.gitxiv.com/posts/Qo2pxsXtTizrwfHoX/orb-slam2-an-open-source-slam-system-for-monocular-stereo)
=======


>>>>>>> 19ee68928feb703f3fe6ab0cbbdf1a22bf7b5377




<<<<<<< HEAD

=======
### Lab

- http://rpg.ifi.uzh.ch/
>>>>>>> 19ee68928feb703f3fe6ab0cbbdf1a22bf7b5377
