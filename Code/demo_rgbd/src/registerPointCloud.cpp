#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>

// 里程计消息
#include <nav_msgs/Odometry.h>
/*
http://docs.ros.org/kinetic/api/nav_msgs/html/msg/Odometry.html

# This represents an estimate of a position and velocity in free space.  
# The pose in this message should be specified in the coordinate frame given by header.frame_id.
# The twist in this message should be specified in the coordinate frame given by the child_frame_id

Header header
string child_frame_id
geometry_msgs/PoseWithCovariance pose
geometry_msgs/TwistWithCovariance twist

关联tf树
Pose 位姿参数
Twist 角速度／线速度
*/

// tf树
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

// 载入相机参数
#include "cameraParameters.h"
#include "pointDefinition.h"

const double PI = 3.1415926;

// 640 * 480 = 307200
const int imagePixelNum = imageHeight * imageWidth;

const int keepSyncCloudNum = 15;
// syncCloudTime在本函数为15个双精度的数组
double syncCloudTime[keepSyncCloudNum] = {0};
// syncCloudArray函数和一下两个参数我不是很懂
/*
syncCloudArray为15个数的点云指针
*/
pcl::PointCloud<pcl::PointXYZ>::Ptr syncCloudArray[keepSyncCloudNum];

/*
syncCloudInd初始化为-1
syncCloudInd更新方程
voDataHandler用到
*/
int syncCloudInd = -1;
int cloudRegInd = 0;

// 三个点云surroundCloud, tempCloud, tempCloud2
// surroundCloud用于储存体素栅格化后的点云数据
pcl::PointCloud<pcl::PointXYZ>::Ptr surroundCloud(new pcl::PointCloud<pcl::PointXYZ>());
pcl::PointCloud<pcl::PointXYZ>::Ptr tempCloud(new pcl::PointCloud<pcl::PointXYZ>());
pcl::PointCloud<pcl::PointXYZ>::Ptr tempCloud2(new pcl::PointCloud<pcl::PointXYZ>());

// Received收到各种信号的参数!间隔固定时间更新
double timeRec = 0;
double rxRec = 0, ryRec = 0, rzRec = 0;
double txRec = 0, tyRec = 0, tzRec = 0;

// cloudCount = (cloudCount + 1) % (cloudSkipNum + 1);
int cloudCount = -1;
const int cloudSkipNum = 5;

int showCount = -1;
const int showSkipNum = 10;

ros::Publisher *surroundCloudPubPointer = NULL;

// voDataHandler函数
void voDataHandler(const nav_msgs::Odometry::ConstPtr& voData)
{
  // 导入voData
  // 读取时间戳
  double time = voData->header.stamp.toSec();

  // 读取roll, pitch, yaw三个角度
 	// Get the matrix represented as roll pitch and yaw about fixed axes XYZ.
  double rx, ry, rz;
  geometry_msgs::Quaternion geoQuat = voData->pose.pose.orientation;
  tf::Matrix3x3(tf::Quaternion(geoQuat.z, -geoQuat.x, -geoQuat.y, geoQuat.w)).getRPY(rz, rx, ry);

  // 读取位置
  double tx = voData->pose.pose.position.x;
  double ty = voData->pose.pose.position.y;
  double tz = voData->pose.pose.position.z;

  // timeRec, syncCloudInd初始值分别为0, -1
  if (time - timeRec < 0.5 && syncCloudInd >= 0) {
    //初始化点结构体
    pcl::PointXYZ point;
    while (syncCloudTime[cloudRegInd] <= time && cloudRegInd != (syncCloudInd + 1) % keepSyncCloudNum) {
      // scaleLast 和 scaleCur
      double scaleLast =  (time - syncCloudTime[cloudRegInd]) / (time - timeRec);
      double scaleCur =  (syncCloudTime[cloudRegInd] - timeRec) / (time - timeRec);
      // 限制两者范围在0~1之间
      if (scaleLast > 1) {
        scaleLast = 1;
      } else if (scaleLast < 0) {
        scaleLast = 0;
      }
      if (scaleCur > 1) {
        scaleCur = 1;
      } else if (scaleCur < 0) {
        scaleCur = 0;
      }

      double rx2 = rx * scaleCur + rxRec * scaleLast;
      double ry2;
      if (ry - ryRec > PI) {
        ry2 = ry * scaleCur + (ryRec + 2 * PI) * scaleLast;
      } else if (ry - ryRec < -PI) {
        ry2 = ry * scaleCur + (ryRec - 2 * PI) * scaleLast;
      } else {
        ry2 = ry * scaleCur + ryRec * scaleLast;
      }
      double rz2 = rz * scaleCur + rzRec * scaleLast;

      double tx2 = tx * scaleCur + txRec * scaleLast;
      double ty2 = ty * scaleCur + tyRec * scaleLast;
      double tz2 = tz * scaleCur + tzRec * scaleLast;

      double cosrx2 = cos(rx2);
      double sinrx2 = sin(rx2);
      double cosry2 = cos(ry2);
      double sinry2 = sin(ry2);
      double cosrz2 = cos(rz2);
      double sinrz2 = sin(rz2);

      pcl::PointCloud<pcl::PointXYZ>::Ptr syncCloudPointer = syncCloudArray[cloudRegInd];
      int syncCloudNum = syncCloudPointer->points.size();
      for (int i = 0; i < syncCloudNum; i++) {
        point = syncCloudPointer->points[i];
        double pointDis = sqrt(point.x * point.x + point.y * point.y + point.z * point.z);
        if (pointDis > 0.3 && pointDis < 5) {
          double x1 = cosrz2 * point.x - sinrz2 * point.y;
          double y1 = sinrz2 * point.x + cosrz2 * point.y;
          double z1 = point.z;

          double x2 = x1;
          double y2 = cosrx2 * y1 + sinrx2 * z1;
          double z2 = -sinrx2 * y1 + cosrx2 * z1;

          point.x = cosry2 * x2 - sinry2 * z2 + tx2;
          point.y = y2 + ty2;
          point.z = sinry2 * x2 + cosry2 * z2 + tz2;

          tempCloud->push_back(point);
        }
      }

      cloudRegInd = (cloudRegInd + 1) % keepSyncCloudNum;
    }

    showCount = (showCount + 1) % (showSkipNum + 1);
    if (showCount != 0) {
      rxRec = rx; ryRec = ry; rzRec = rz;
      txRec = tx; tyRec = ty; tzRec = tz;
      timeRec = time;
      return;
    }

    int surroundCloudNum = surroundCloud->points.size();
    for (int i = 0; i < surroundCloudNum; i++) {
      point = surroundCloud->points[i];

      double xDiff = point.x - tx;
      double yDiff = point.y - ty;
      double zDiff = point.z - tz;

      double pointDis = sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
      if (pointDis < 50) {
        tempCloud->push_back(point);
      }
    }

    surroundCloud->clear();
    // VoxelGrid体素栅格滤波基本
    // 初始化滤波器
    pcl::VoxelGrid<pcl::PointXYZ> downSizeFilter;
    // 对于tempCloud进行输入
    downSizeFilter.setInputCloud(tempCloud);
    //设置栅格大小
    downSizeFilter.setLeafSize(0.2, 0.2, 0.2);
    // 滤波处理储存输出
    downSizeFilter.filter(*surroundCloud);
    tempCloud->clear();

    sensor_msgs::PointCloud2 surroundCloud2;
    pcl::toROSMsg(*surroundCloud, surroundCloud2);
    surroundCloud2.header.frame_id = "/camera_init";
    surroundCloud2.header.stamp = voData->header.stamp;
    surroundCloudPubPointer->publish(surroundCloud2);
  }

  rxRec = rx; ryRec = ry; rzRec = rz;
  txRec = tx; tyRec = ty; tzRec = tz;
  timeRec = time;
}

// syncCloudHandler函数
void syncCloudHandler(const sensor_msgs::Image::ConstPtr& syncCloud2)
{
  cloudCount = (cloudCount + 1) % (cloudSkipNum + 1);
  
  // cloudCount等于0时进入下面代码
  if (cloudCount != 0) {
    return;
  }

  double time = syncCloud2->header.stamp.toSec();

  syncCloudInd = (syncCloudInd + 1) % keepSyncCloudNum;
  syncCloudTime[syncCloudInd] = time;

  tempCloud2->clear();
  // 生成单独点
  pcl::PointXYZ point;
  // ?下面不是很懂
  const float* syncCloud2Pointer = reinterpret_cast<const float*>(&syncCloud2->data[0]);

  // 逐个像素点进行解析
  for (int i = 0; i < imagePixelNum; i++) {
    float val = syncCloud2Pointer[i];

    // 第xd行,第yd个像素
    int xd = i % imageWidth;
    int yd = int(i / imageWidth);

    /*
    0: fx 525.0
    2: cx 319.5 = 320
    4: fy 525.0
    5: cy 239.5 = 240
    */
    // 内参矩阵 cameraMatrix[9] = {fx, 0, cx, 0, fy, cy, 0, 0, 1}
    double ud = (kDepth[2] - xd) / kDepth[0];
    double vd = (kDepth[5] - yd) / kDepth[4];

    point.z = val;
    point.x = ud * val;
    point.y = vd * val;

    if (point.z > 0.3 && point.z < 7) {
      tempCloud2->push_back(point);
    }
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr syncCloudPointer = syncCloudArray[syncCloudInd];
  syncCloudPointer->clear();

  pcl::VoxelGrid<pcl::PointXYZ> downSizeFilter;
  downSizeFilter.setInputCloud(tempCloud2);
  downSizeFilter.setLeafSize(0.1, 0.1, 0.1);
  downSizeFilter.filter(*syncCloudPointer);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "registerPointCloud");
  ros::NodeHandle nh;

  // 循环读入暂存区
  for (int i = 0; i < keepSyncCloudNum; i++) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr syncCloudTemp(new pcl::PointCloud<pcl::PointXYZ>());
    syncCloudArray[i] = syncCloudTemp;
  }

  // 读取数据
  ros::Subscriber voDataSub = nh.subscribe<nav_msgs::Odometry> ("/cam2_to_init", 5, voDataHandler);

  // 同步数据
  ros::Subscriber syncCloudSub = nh.subscribe<sensor_msgs::Image>
                                 ("/camera/depth_registered/image", 1, syncCloudHandler);

  // 发布数据
  ros::Publisher surroundCloudPub = nh.advertise<sensor_msgs::PointCloud2> ("/surround_cloud", 1);

  // 周围点云指针
  surroundCloudPubPointer = &surroundCloudPub;

  ros::spin();

  return 0;
}