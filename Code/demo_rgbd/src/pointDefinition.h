#ifndef point_definition_h
#define point_definition_h

#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PointStamped.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
// 体素栅格滤波
#include <pcl/filters/voxel_grid.h>
// kdtree寻找临近树！
#include <pcl/kdtree/kdtree_flann.h>

// 图片点结构体，u v分别为在投影平面的坐标
struct ImagePoint {
     float u, v;
     int ind;
};

// registered点云格式
POINT_CLOUD_REGISTER_POINT_STRUCT (ImagePoint,
                                   (float, u, u)
                                   (float, v, v)
                                   (int, ind, ind))

/*---------------获取深度信息之后------------------------*/

// 深度点
struct DepthPoint {
     float u, v;
     float depth;
     int label;
     int ind;
};

// registered点云格式
POINT_CLOUD_REGISTER_POINT_STRUCT (DepthPoint,
                                   (float, u, u)
                                   (float, v, v)
                                   (float, depth, depth)
                                   (int, label, label)
                                   (int, ind, ind))

#endif