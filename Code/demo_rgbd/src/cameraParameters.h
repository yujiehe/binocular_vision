#ifndef camera_parameters_h
#define camera_parameters_h

#include <opencv/cv.h>
#include <sensor_msgs/Image.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
//#include "opencv2/contrib/contrib.hpp"
#include <opencv2/calib3d/calib3d.hpp>

#include <cv_bridge/cv_bridge.h>

// 像素大小4640*480
const int imageWidth = 640;
const int imageHeight = 480;

// 照片参数
 
/*
registerPC.cpp
    double ud = (kDepth[2] - xd) / kDepth[0];
    double vd = (kDepth[5] - yd) / kDepth[4];

0: fx
2: cx
4: fy
5: cy
*/

// 内参矩阵 cameraMatrix[9] = {fx, 0, cx, 0, fy, cy, 0, 0, 1}
double kImage[9] = {525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0};
double kDepth[9] = {525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0};

#endif

