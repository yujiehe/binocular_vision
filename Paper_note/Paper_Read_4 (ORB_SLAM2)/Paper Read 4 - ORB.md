# Paper Read IV - ORB

> Week from 20180923 TO 20181015s

## ORB-SLAM A Versatile and Accurate Monocular SLAM System

> *ORB-SLAM: Tracking and Mapping Recognizable Features* 为较旧版本论文
>
> 全文翻译版本：
>
> -　[【泡泡机器人翻译专栏】ORB-SLAM：精确多功能单目SLAM系统(一)](https://www.sohu.com/a/161095723_715754)
> -　[【泡泡机器人翻译专栏】ORB-SLAM：精确多功能单目SLAM系统(二)](https://www.sohu.com/a/161346283_715754)
> -　[ORB-SLAM2](http://www.sohu.com/a/154011668_715754)
> -　https://www.sohu.com/a/161346283_715754





### 





### 0. Abstract



### I. Intro



### II. MAP



### III. PLACE RECOGNITION



### IV. TRACKING



### V. LOCAL MAPPING



### VI. LOOP CLOSER



### VII. EXPERIMENTS



### VIII. CONCLUSION



## ORB-SLAM2 An Open-Source SLAM System for Monocular, Stereo and RGB-D Cameras





### Key Idea

