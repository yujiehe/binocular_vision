# Paper Read I

> 20180818

## Design of an Autonomous Racecar: Perception, State Estimation and System Integration

> ICRA best student paper

### Intro

- Intention

This paper presents the state estimation, LiDAR SLAM, and localization systems that were integrated in flüela.

- sensors and processors

1. a 3D LiDAR
2. a self-developed visual-inertial stereo camera system
3. velocity sensors
4. an Inertial Navigation System (INS) combining an IMU and a GPS for state estimation
5. two computing units running Robot Operating System (ROS)
6. a real-time capable Electronic Control Unit (ECU).

- target

to reach flüela’s full potential

the track must be known at least 2s ahead. 

At high speeds, this requires a perception horizon that exceeds the sensors’ range. The car must thus drive carefully to discover and map the track. This mode will later be referred
to as **SLAM Mode**. 

Once the map is known, the car can drive in **Localization Mode** which can exploit the advantage of planning on the previously mapped race-track.

### flüela Driverless

- System architecture



## A Universal Grid Map Library: Implementation and Use Case for Rough Terrain Navigation

> A Universal Grid Map Library: Implementation and Use Case for Rough Terrain Navigation
>
> http://github.com/ethz-asl/grid_map
> http://github.com/ethz-asl/elevation_mapping

Mobile ground robots traditionally **move on flat terrain** and their <u>mapping, planning, and control algorithms</u> are typically developed for a **two-dimensional abstraction of the environment.**

in rough terrain: most popular approach is an elevation map



