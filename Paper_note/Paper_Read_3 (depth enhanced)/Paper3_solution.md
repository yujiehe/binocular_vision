- OpenCV

set(CMAKE_PREFIX_PATH "//usr/local/")

set(CMAKE_PREFIX_PATH "/usr/local/")

- eigen3

```
catkin_package() DEPENDS on 'Eigen3' but neither 'Eigen3_INCLUDE_DIRS' nor
  'Eigen3_LIBRARIES' is defined.
```

直接写"/usr/include/eigen3"

include_directories( "/usr/include/eigen3" )

---

# 测试代码方法

## OpenCV

## 付凌方法

```
Ubuntu16.04 安装 opencv3.4.2
1. sudo apt-get install cmake
2. sudo apt-get install build-essential libgtk2.0-dev libavcodec-dev libavformat-dev libjpeg.dev libtiff4.dev libswscale-dev libjasper-dev
(opencv的一些库)
3. mkdir build (当前路径是~/opencv-3.4.2)
4. cd build
5. cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..
(编译opencv)
6. sudo make
7. sudo make install
8. sudo gedit /etc/ld.so.conf.d/opencv.conf （配置环境，将opencv的库添加到路径中）
里面输入：
/usr/local/lib
save
9. sudo ldconfig
10. sudo gedit /etc/bash.bashrc
添加：
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig  
export PKG_CONFIG_PATH 
save
11. source /etc/bash.bashrc 
12. sudo updatedb  
---
查看版本：
pkg-config --modversion opencv
```


## [ubuntu 安装使用多版本opencv](https://blog.csdn.net/heyijia0327/article/details/54575245)

### 安装opencv2到指定路径

1.下载opencv2.4.13,链接为[opencv sourceforge](https://sourceforge.net/projects/opencvlibrary/?source=typ_redirect)上的网页,自己选择合适的版本。 
 2.解压到指定文件夹, 比如~/opencv2.4.13 
 3.打开终端，切换到该文件集下，并创建文件夹如下：

```c++
cd ~/opencv2.4.13
mkdir build
cd build
mkdir installed
```

创建的installed文件集就是我们安装opencv2.4.13的路径。 
 4.使用cmake的时候指定opencv的安装路径，因为之前已经默认安装了opencv3，所以这里必须指定安装路径，比如我打算安装在刚刚创建的installed文件下。接着上面mkdir installed命令后，输入如下cmake 命令：

```c++
sudo apt-get install build-essential libgtk2.0-dev libavcodec-dev libavformat-dev libjpeg.dev libtiff4.dev libswscale-dev libjasper-dev
cmake -D CMAKE_INSTALL_PREFIX=~/opencv-2.4.13/build/installed -DCMAKE_BUILD_TYPE="Release" ..
```

5.编译安装

```c++
make
make install
```

以上安装过程参考 
 <http://code.litomisky.com/2014/03/09/how-to-have-multiple-versions-of-the-same-library-side-by-side/>

### 同理安装opencv3.1

如果你电脑已经有了opencv2.4，想装一个opencv3，也要显式的指定安装路径，如下。

```c++
cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=~/slam_Thirdparty/opencv-3.1.0/release/installed -D OPENCV_EXTRA_MODULES_PATH=~/slam_Thirdparty/opencv_contrib-3.1.0/modules ..1
```

### 使用多版本opencv

在写CmakeList.txt，如果只有一个版本的opencv,我们一般直接使用

```c++
FIND_PACKAGE(OpenCV REQUIRED)1
```

现在如果是使用默认安装的opencv3.1,则使用使用上面的指令就足够了。如果使用安装我们自己指定路径的opencv2.4.13，则在上面指令前面加上如下指令：

```c++
set(CMAKE_PREFIX_PATH "~/opencv-2.4.13/build/installed/")
```

## [ubuntu16.04 opencv多版本管理与切换](https://blog.csdn.net/u012986684/article/details/77490824)

### **设置管理**

一般情况下不能直接opencv 的环境写到zshrc,所以我们需要手动进行切换

当你需要使用opencv 2.4.9的话,在终端输入:

```plain
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/opencv2/lib/pkgconfig
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/opencv2/lib
```

生效 

```plain
sudo ldconfig
```

当你需要使用opencv 3.1.0的话,在终端输入:

```plain
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/opencv3/lib/pkgconfig
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/opencv3/lib
```



其他注意

---

自行探索

https://blog.csdn.net/zhuoyueljl/article/details/78504764

https://github.com/ethz-asl/opencv2_catkin

https://github.com/catkin/catkin_simple

https://blog.csdn.net/bigdog_1027/article/details/79092263



### 错误

CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
Please set them or make sure they are set and tested correctly in the CMake files:
CUDA_nppi_LIBRARY (ADVANCED)

https://blog.csdn.net/syoung9029/article/details/56538503

尝试办法

https://github.com/jayrambhia/Install-OpenCV/blob/master/Ubuntu/2.4/opencv2_4_10.sh

无法解决

https://stackoverflow.com/questions/46584000/cmake-error-variables-are-set-to-notfound

照着瞎改解决

```
modules/gpu/CMakeFiles/opencv_gpu.dir/build.make:18090: recipe for target 'modules/gpu/CMakeFiles/opencv_gpu.dir/src/graphcuts.cpp.o' failed
make[2]: *** [modules/gpu/CMakeFiles/opencv_gpu.dir/src/graphcuts.cpp.o] Error 1
CMakeFiles/Makefile2:5063: recipe for target 'modules/gpu/CMakeFiles/opencv_gpu.dir/all' failed
make[1]: *** [modules/gpu/CMakeFiles/opencv_gpu.dir/all] Error 2
```

https://blog.csdn.net/Dillon2015/article/details/79250169

解决,然后99%出错

```
modules/contrib/CMakeFiles/opencv_contrib.dir/build.make:110: recipe for target 'modules/contrib/CMakeFiles/opencv_contrib.dir/src/rgbdodometry.cpp.o' failed
make[2]: *** [modules/contrib/CMakeFiles/opencv_contrib.dir/src/rgbdodometry.cpp.o] Error 1
make[2]: *** Waiting for unfinished jobs....
CMakeFiles/Makefile2:6239: recipe for target 'modules/contrib/CMakeFiles/opencv_contrib.dir/all' failed
make[1]: *** [modules/contrib/CMakeFiles/opencv_contrib.dir/all] Error 2
```

https://blog.csdn.net/poulang5786/article/details/79249755

修改eigen3地址

/usr/local/include/eigen3/unsupported

最后okay

```
cmake -D CMAKE_BUILD_TYPE=Release -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules/ -D CMAKE_INSTALL_PREFIX=/usr/local/ ..
make -j
sudo make install
```

如果需要编译opencv2

set(CMAKE_PREFIX_PATH "/usr/local/")

## iSAM

http://people.csail.mit.edu/kaess/isam/

git clone

```
https://github.com/ori-drs/isam
```

To install all required packages in Ubuntu 9.10 and later, simply type:

```
sudo apt-get install cmake libsuitesparse-dev libeigen3-dev libsdl1.2-dev doxygen graphviz
```

Note that CHOLMOD is contained in SuiteSparse. On Mac OS X, SuiteSparse and SDL are available through MacPorts:

```
sudo port install suitesparse libsdl
```

### Installation

Compile with:

```
make
```

Directory structure:

- `isamlib/` contains the source code for the iSAM library
- `include/` contains the header files for the iSAM library
- `isam/` contains the source code for the iSAM executable
- `examples/` contains examples for using different components of the iSAM library
- `doc/` contains the documentation (after calling "make doc")
- `misc/` contains code referenced from publications
- `data/` contains example data files
- `lib/` contains the actual library "libisam.a"
- `bin/` contains the main executable "isam"

Usage example: 确认安装

```
bin/isam -G data/sphere400.txt
```

For more usage information: 相关快捷操作

```
bin/isam -h
```

Install the library in your system with:

```
make install
```

### 相关信息

位置

```
/usr/local/include/isam
```

