[TOC]

---

## 运行

```shell
cd /home/philhe/Desktop/test-1youngguncho/
source devel/setup.zsh
roslaunch demo_rgbd demo_rgbd.launch
# 或者下面
cd /home/philhe/Desktop/test-1youngguncho/src/demo_rgbd
roslaunch demo_rgbd.launch

cd /home/philhe/Desktop/VO_src/demo_rgbd/nsh_east_sparse_depth

rosbag play nsh_east_sparse_depth_part1.bag -r 0.5
```
- rviz界面

原始界面: /camera/rgb/image_rect

深度界面:/ camera/depth_registered/image

![Software Arch](./Software Arch.png)

---

## 结果

### [youngguncho](https://github.com/youngguncho)/[**demo_rgbd**](https://github.com/youngguncho/demo_rgbd)

```
Kinetic version of DEMO. 
Compile done but lost pose estimation in visualOdometry.cpp
```

编译warning

```
** WARNING ** io features related to pcap will be disabled
** WARNING ** io features related to png will be disabled
-- The imported target "vtkRenderingPythonTkWidgets" references the file
   "/usr/lib/x86_64-linux-gnu/libvtkRenderingPythonTkWidgets.so"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   "/usr/lib/cmake/vtk-6.2/VTKTargets.cmake"
but not all the files it references.

-- The imported target "vtk" references the file
   "/usr/bin/vtk"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   "/usr/lib/cmake/vtk-6.2/VTKTargets.cmake"
but not all the files it references.
```

可以测试，但丢失了估计在rviz中无法正常显示？

![demo_rgbd_rqt1](./demo_rgbd_rqt1.png)

```
[featureTracking-1] process has died [pid 31773, exit code -11, cmd /home/philhe/Desktop/test-1youngguncho/devel/lib/demo_rgbd/featureTracking __name:=featureTracking __log:=/home/philhe/.ros/log/85316f74-cecf-11e8-b325-f0038c0ab1cd/featureTracking-1.log].
log file: /home/philhe/.ros/log/85316f74-cecf-11e8-b325-f0038c0ab1cd/featureTracking-1*.log

opencv报错

OpenCV Error: The function/feature is not implemented (Unknown/unsupported array type) in getMat, file /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp, line 1319
terminate called after throwing an instance of 'cv::Exception'
  what():  /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp:1319: error: (-213) Unknown/unsupported array type in function getMat
```

https://answers.ros.org/question/212860/at-an-impasse-using-opencv/

```
find_package(OpenCV REQUIRED)
Replace it with
find_package(OpenCV 2 REQUIRED)
I worked whole day to find the solution
```

同样报错

```
OpenCV Error: The function/feature is not implemented (Unknown/unsupported array type) in getMat_, file /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp, line 1319
terminate called after throwing an instance of 'cv::Exception'
  what():  /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp:1319: error: (-213) Unknown/unsupported array type in function getMat_

[featureTracking-2] process has died [pid 4981, exit code -6, cmd /home/philhe/Desktop/test-1youngguncho/devel/lib/demo_rgbd/featureTracking __name:=featureTracking __log:=/home/philhe/.ros/log/8d5ff15c-cf51-11e8-9841-f0038c0ab1cd/featureTracking-2.log].
log file: /home/philhe/.ros/log/8d5ff15c-cf51-11e8-9841-f0038c0ab1cd/featureTracking-2*.log
```

怀疑cv_bridge有问题

---

### [tuandle](https://github.com/tuandle) / [demo_rgbd](https://github.com/tuandle/demo_rgbd)

#### 编译过程

报错

```
The imported target "vtkRenderingPythonTkWidgets" references the file
   "/usr/lib/x86_64-linux-gnu/libvtkRenderingPythonTkWidgets.so"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   "/usr/lib/cmake/vtk-6.2/VTKTargets.cmake"
but not all the files it references.

-- The imported target "vtk" references the file
   "/usr/bin/vtk"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   "/usr/lib/cmake/vtk-6.2/VTKTargets.cmake"
but not all the files it references.

Could NOT find PCL_APPS (missing:  PCL_APPS_LIBRARY) 
```

完全编译,待测试

https://groups.google.com/forum/#!topic/fenics-support/Yx4utE100xA

sudo apt-get install python-vtk 会发生冲突

https://github.com/RobotLocomotion/drake/issues/2624

先测试！不太行

https://stackoverflow.com/questions/37369369/compiling-pcl-1-7-on-ubuntu-16-04-errors-in-cmake-generated-makefile

似乎有解决方法,`sudo apt install libproj-dev`

尝试进行切换,启动后报错,feature节点死亡

```
OpenCV Error: Assertion failed (0 <= i && i < (int)v.size()) in getMat_, file /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp, line 1275
terminate called after throwing an instance of 'cv::Exception'
  what():  /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp:1275: error: (-215) 0 <= i && i < (int)v.size() in function getMat_

[featureTracking_rgbd-1] process has died [pid 1514, exit code -6, cmd /home/philhe/catkin_ws/devel/lib/demo_rgbd/featureTracking_rgbd __name:=featureTracking_rgbd __log:=/home/philhe/.ros/log/85316f74-cecf-11e8-b325-f0038c0ab1cd/featureTracking_rgbd-1.log].
log file: /home/philhe/.ros/log/85316f74-cecf-11e8-b325-f0038c0ab1cd/featureTracking_rgbd-1*.log

```

再次编译

```
OpenCV Error: Assertion failed (0 <= i && i < (int)vv.size()) in getMat_, file /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp, line 1266
terminate called after throwing an instance of 'cv::Exception'
  what():  /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp:1266: error: (-215) 0 <= i && i < (int)vv.size() in function getMat_

[featureTracking_rgbd-2] process has died [pid 18556, exit code -6, cmd /home/philhe/Desktop/test-2tuandle/devel/lib/demo_rgbd/featureTracking_rgbd __name:=featureTracking_rgbd __log:=/home/philhe/.ros/log/583acc20-cf5d-11e8-9841-f0038c0ab1cd/featureTracking_rgbd-2.log].
log file: /home/philhe/.ros/log/583acc20-cf5d-11e8-9841-f0038c0ab1cd/featureTracking_rgbd-2*.log
```

### [PyojinKim](https://github.com/PyojinKim)/**demo_rgbd**

http://pyojinkim.me/

无法运行

## 解决方案

- 其中一个是继续使用docker进行相关的编译
- 另外一个是换系统

位置识别

---

[TOC]