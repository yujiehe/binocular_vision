Looking Forward to Your Help in demo_rgbd

Dear Yoshua Nava,

Hello, I'm a junior student major in Mechatronics coming from Tongji Univ., Shanghai. CHINA. I just read Ji Zhang's Paper **Real-time depth enhanced monocular odometry** and tried to run the codes which downloaded from your repo https://github.com/PyojinKim/demo_rgbd.

My setup is Ubuntu16.04, ROS Kinetic, OpenCV2.4.10. But when I catkin_make the package and try to test dataset from http://wiki.ros.org/demo_rgbd, I came into the followings errors.

```
OpenCV Error: Assertion failed (0 <= i && i < (int)v.size()) in getMat_, file /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp, line 1275
terminate called after throwing an instance of 'cv::Exception'
  what():  /tmp/binarydeb/ros-kinetic-opencv3-3.3.1/modules/core/src/matrix.cpp:1275: error: (-215) 0 <= i && i < (int)v.size() in function getMat_

[featureTracking_rgbd-1] process has died [pid 1514, exit code -6, cmd /home/philhe/catkin_ws/devel/lib/demo_rgbd/featureTracking_rgbd __name:=featureTracking_rgbd __log:=/home/philhe/.ros/log/85316f74-cecf-11e8-b325-f0038c0ab1cd/featureTracking_rgbd-1.log].
log file: /home/philhe/.ros/log/85316f74-cecf-11e8-b325-f0038c0ab1cd/featureTracking_rgbd-1*.log
```

I'm a freshman in this area, so I hope to seek for your help in this question.

