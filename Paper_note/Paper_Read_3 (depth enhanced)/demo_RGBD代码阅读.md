[TOC]

---

 **(小技巧记录：如果你的工作空间下包很多，每次都使用catkin_make的话效率十分低下，因为这种编译方法会编译工作空间下的所有的包，特别是我们在调试程序是会经常修改CMakeLists.txt文件里的内容，这样每次都会要编译整个工作空间，那么所以我们可以使用ROS的catkin_Make的功能编译一个或者多个包，具体的命令是：catkin_make -DCATKIN_WHITELIST_PACKAGES=" 你的包名")，例如：catkin_make -DCATKIN_WHITELIST_PACKAGES="ros_slam"**

# 代码阅读

## `/include`

### cameraParameters.h

#### 主要内容

参数标定文件

**cameraMatrix** – Output 3x3 floating-point camera matrix ![A = \vecthreethree{f_x}{0}{c_x}{0}{f_y}{c_y}{0}{0}{1}](https://docs.opencv.org/2.4.1/_images/math/c3dee78d26409d9ec88d2eee5b3342a664a4b960.png)

```c++
// 像素大小4640*480
const int imageWidth = 640;
const int imageHeight = 480;

// 相机内参数矩阵
// 内参矩阵 cameraMatrix[9] = {fx, 0, cx, 0, fy, cy, 0, 0, 1}
double kImage[9] = {525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0};
double kDepth[9] = {525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0};
```

#### 参考资料

https://docs.opencv.org/2.4.1/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html

https://blog.csdn.net/aoulun/article/details/78768570

### pointDefinition.h

#### 头文件

- `#include <pcl/filters/voxel_grid.h>`

使用VoxelGrid滤波器对点云进行下采样

 使用体素化网格方法实现下采样，即**减少点的数量 减少点云数据，并同时保存点云的形状特征**，在提高配准，曲面重建，形状识别等算法速度中非常实用，PCL是实现的VoxelGrid类通过输入的点云数据创建一个三维体素栅格，容纳后每个体素内用体素中所有点的重心来近似显示体素中其他点，这样该体素内所有点都用一个重心点最终表示，对于所有体素处理后得到的过滤后的点云，这种方法比用体素中心逼近的方法更慢，但是对于采样点对应曲面的表示更为准确。

```c++
// voxel_grid.cpp
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

int
main (int argc, char** argv)
{
  pcl::PCLPointCloud2::Ptr cloud (new pcl::PCLPointCloud2 ());
  pcl::PCLPointCloud2::Ptr cloud_filtered (new pcl::PCLPointCloud2 ());

  //点云对象的读取
  pcl::PCDReader reader;
 
  reader.read ("table_400.pcd", *cloud);    //读取点云到cloud中

  std::cerr << "PointCloud before filtering: " << cloud->width * cloud->height 
       << " data points (" << pcl::getFieldsList (*cloud) << ").";

  /******************************************************************************
  创建一个叶大小为1cm的pcl::VoxelGrid滤波器，
**********************************************************************************/
  pcl::VoxelGrid<pcl::PCLPointCloud2> sor;  //创建滤波对象
  sor.setInputCloud (cloud);            //设置需要过滤的点云给滤波对象
  sor.setLeafSize (0.01f, 0.01f, 0.01f);  //设置滤波时创建的体素体积为1cm的立方体
  sor.filter (*cloud_filtered);           //执行滤波处理，存储输出

  std::cerr << "PointCloud after filtering: " << cloud_filtered->width * cloud_filtered->height 
       << " data points (" << pcl::getFieldsList (*cloud_filtered) << ").";

  pcl::PCDWriter writer;
  writer.write ("table_scene_lms400_downsampled.pcd", *cloud_filtered, 
         Eigen::Vector4f::Zero (), Eigen::Quaternionf::Identity (), false);

  return (0);
}
```

从输出的结果可以看出，过滤后的数据量大大减小了。

- `#include <pcl/kdtree/kdtree_flann.h>`

点云数据处理中最为核心的问题就是建立离散点间的拓扑关系，实现基于邻域关系的快速查找。

k-d树 （k-dimensional树的简称），是一种分割k维数据空间的数据结构。主要应用于多维空间关键数据的搜索（如：范围搜索和最近邻搜索）。K-D树是二进制空间分割树的特殊的情况。用来组织表示K维空间中点的几何，是一种带有其他约束的二分查找树

## `/src`

### featureTracking.cpp

#### 变量声明

[CvSize结构](https://blog.csdn.net/sphone89/article/details/6097536)

```c++
/*
typedef struct CvSize {
int width; /* 矩形宽度，单位为象素 */
int height; /* 矩形高度，单位为象素 */
}CvSize;
*/
```

[IplImage, CvMat, Mat 的关系](https://www.cnblogs.com/summerRQ/articles/2406109.html)

opencv2.1版本之前使用IplImage*数据结构来表示图像，2.1之后的版本使用图像容器Mat来存储。IplImage结构体如下所示。

[OpenCV中cvLoadImage()函数和cvCreateImage()函数的异同之处！](https://blog.csdn.net/qq_32211827/article/details/68060700)

[opencv笔记——cvCreateImage函数说明](https://blog.csdn.net/breeze5428/article/details/30050327)

`IplImage* cvCreateImage(CvSize cvSize(int width, int height), int depth, int channels);`

- 实际代码块

```c++
// 640 * 480 定义宽为640象素，高为480象素的矩形图片
const int imagePixelNum = imageHeight * imageWidth;
CvSize imgSize = cvSize(imageWidth, imageHeight);
// 生成指针IplImage,并使用create函数
IplImage *imageCur = cvCreateImage(imgSize, IPL_DEPTH_8U, 1);
```

与很多OpenCV结构类似，构造函数cvMat()可以创建CvMat结构，实际上不分配存储空间，仅创建头结构（与cvInitMatHeader()类似）。这些方法对与存取到处散放的数据很有用，可以让矩阵头指向这些数据，实现对这些数据的打包，并用操作矩阵的函数来处理这些数据。来看代码怎么说：

`CvMat kMat = cvMat(3, 3, CV_64FC1, kImage);`

CvPoint2D32f 二维坐标下的点，类型为浮点

```
CvPoint2D32f *featuresCur = new CvPoint2D32f[2 * MAXFEATURENUM];
```

[ROS关于cv_brige的使用](https://www.cnblogs.com/li-yao7758258/p/6637079.html)

```c++
cv_bridge::CvImage bridge;
```





#### 主函数

### registerPointCloud.cpp

#### 头文件

#### 其它函数

- `void voDataHandler`

数据结构(const nav_msgs::Odometry::ConstPtr& voData)

```c++
// getRPY (tfScalar &roll, tfScalar &pitch, tfScalar &yaw, unsigned int solution_number=1) const
tf::Matrix3x3(tf::Quaternion(geoQuat.z, -geoQuat.x, -geoQuat.y, geoQuat.w)).getRPY(rz, rx, ry);
```

Get the matrix represented as roll pitch and yaw about fixed axes XYZ. More...

#### 主函数

- `void imageDataHandler(const sensor_msgs::Image::ConstPtr& imageData) `

ros下的sensor_msgs::Image对此的定义

```c++
# This message contains an uncompressed image
# (0, 0) is at top-left corner of image
Header header        # Header timestamp should be acquisition time of image
                     # Header frame_id should be optical frame of camera
                     # origin of frame should be optical center of camera
                     # +x should point to the right in the image
                     # +y should point down in the image
                     # +z should point into to plane of the image
                     # If the frame_id here and the frame_id of the CameraInfo
                     # message associated with the image conflict
                     # the behavior is undefined
uint32 height         # image rows number 
uint32 width          # image columns number 

string encoding       # Encoding of pixels -- channel meaning, ordering, size
                      # taken from the list of strings in include/sensor_msgs/image_encodings.h

uint8 is_bigendian    # is this data bigendian?是否连续
uint32 step           # Full row length in bytes
uint8[] data          # actual matrix data, size is (step * rows)
```

[cvResize用法](https://blog.csdn.net/shandianling/article/details/6439590)

cvCornerHarris 角点检测

```从++
  // 缩放对象?根据上述代码应该为两倍
  cvResize(imageLast, imageShow);
  cvCornerHarris(imageShow, harrisLast, 3);
```



## 附录

- 关于cvLoadImage与cvCreateImage的应用

```c++
include<opencv2\opencv.hpp>

int main()
{
	IplImage* srcImage=cvLoadImage(".\face2.jpg",1);				//注意第二个参数的值
	CvSize ImageSize;
    ImageSize.height=srcImage->height;
    ImageSize.width=srcImage->width;
    IplImage* Image = cvCreateImage( ImageSize,srcImage->depth,1); //注意第三个参数的值
    cvCvtColor(srcImage,Image,CV_RGB2GRAY);	  //这里的srcImage必须是指向三通道的彩色图片的指针，Image必须是单通道的灰度图片指针，不然会出现下面的错误
    cvCanny(srcImage,Image,3,9,3);            //进行简单的边缘检测

    cvNamedWindow("原图");
    cvNamedWindow("效果图");

    cvShowImage("原图",srcImage);
    cvShowImage("效果图",Image);

    cvWaitKey(0);
    cvReleaseImage(&srcImage);
    cvReleaseImage(&Image);
    cvDestroyWindow("原图");
    cvDestroyWindow("效果图");
    return 0;
}
```

- V-LOAM 源码解析（一）

https://www.cnblogs.com/zhchp-blog/p/8735132.html

- [ROS关于cv_brige的使用](https://www.cnblogs.com/li-yao7758258/p/6637079.html)