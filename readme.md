# Binocular vision learning plan

## 0. From ChanghongFU

制定下计划，一步步达到自己的理想成果，比如9-10月选好双目相机（系统），熟悉常用KITTI评估系统等，11月-...

## 1. My Plan
**18**

- 9：相关论文/专利收集，双目系统选择，并开始学习电子基础
- 10：确定双目，并使用KITTI或EUROC MAV数据集评估，开始专利书写
- 11：继续数据集测试评估与专利书写，并进行相关元件选型
- 12：继续专利书写（预期完成），开始进行相关选型，完成电路工程设计

**19**

- 1~2：争取在寒假结束前完成初稿，同时进行论文阅读以及代码开发
- 3：完成初稿开始标定测试，同时进行论文阅读以及代码开发
- 4：继续完善，同时进行论文阅读以及代码开发
- 5：继续迭代，论文阅读以及代码开发

### 1.1 Focus on

- Binocular vision
- Object Tracking
- Object detection
- IMU
- LiDAR

`TO BE CONTIUED`

## 2. Codings

- ros
- c++
- web suits
- julia

`TO BE CONTIUED`

## 3. Tools

- Matlab
- **AD**esigner

## 4. Library

**数据集**

- [KITTI](http://www.cvlibs.net/datasets/kitti/eval_object.php)
- [Cityscapes](https://www.cityscapes-dataset.com/dataset-overview/)
- [EUROC MAV](https://projects.asl.ethz.ch/datasets/doku.php?id=kmavvisualinertialdatasets)

**库文件**

- eigen
- KITTI
- PCL
- OpenCV
- CUDA
- OpenAI
- [Visual Tracker Benchmark](http://cvlab.hanyang.ac.kr/tracker_benchmark/)
- [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)

`TO BE CONTIUED`

## 5. Hardware

- Odroid
- [XIAOMI STANDARD](http://www.myntai.com/mynteye/standard)
- [zed](https://www.stereolabs.com/zed/)


## 6. Books & Papers to Read

- Multiple View Geometry
- 视觉SLAM十四讲
- State Estimation for Robotics

`TO BE CONTIUED`

## 7. Results

- Hardwares to realize
- Patents to apply
- Papers to publish
- Codes to share

`TO BE CONTIUED`